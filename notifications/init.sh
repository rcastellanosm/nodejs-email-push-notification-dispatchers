#!/bin/sh

export MAILGUN_SECRET=$(cat /run/secrets/mailgun_secret)
export AWS_SECRET_ACCESS_KEY=$(cat /run/secrets/amazon_sns_access_token)
export AWS_ACCESS_KEY_ID=$(cat /run/secrets/amazon_sns_key_id)

yarn server
