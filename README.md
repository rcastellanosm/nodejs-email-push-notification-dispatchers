## Notifications
NodeJS rabbitmq consumers for managing queues for stack

This service has two tasks:
* Send notifications via email, push or sms to a final addressee
* Subscribe / Unsubscribe an addressee from topics or distribution lists

PD: An addressee can be (mailbox, distribution_list, phone_number, sns_topic, device_arn)

See: ADD_TO_STACK.md for instructions on how to add to your stack services

#### Environment vars and secrets
Services vars are included in a **.env.template** file inside notification service

#### Queues
These queues are pre-setup on RabbitMQ Service. 

* notifications.dispatch (routingKey: dispatch)
* notifications.manage (routingKey: manage)

Those can be reached through the Connector Exchange

* notifications.connectorExchange (exchange for producers)

#### Payload Examples 
The following examples show format of messages persisted on RabbitMQ queues by producer's backend

* Email Notification 
    ```text 
    Queue: dispatch
    ```
    ```json
    {
    "type": "email",
    "addressee":"{mail_inbox|distribution_list}",
    "data": {
        "title":"{title}",
        "html":"{html_raw}",
        "o:tag" : ["{tag1}", "{tag2}"]
        }
    }
    ```

* PUSH Notification 
    ```text 
    Queue: dispatch
    ```
    ```json
    {
    "type": "push",
    "mode" : "direct|topic",
    "addressee":"{topic_arn}",
    "data": {
        "title":"{push_title}",
        "content":"{push_content}",
        "intent": "{intent}",
        "image": "{uri}"
        }
    }
    ```

* Subscribe / Unsubscribe from distribution list or topic 
    ```text 
    Queue: manage
    ```
    ```json
    {
      "type":  "{email|push}",
      "mode":  "{subscribe|unsubscribe}",
      "list": "{topic_arn|distribution_list}",
      "addressee": "{device_endpoint|email}"
    }
    ```
