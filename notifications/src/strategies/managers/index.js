const managers = {
  email: require('./email'),
  push: require('./push'),
};
const _ = require('lodash');
const logger = require('../../config/logger');

const useStrategy = (payload) => {
  let message = JSON.parse(payload);
  let strategy = Object.keys(managers).filter(function (manager) {
    return manager === message.type;
  });

  if (!_.isEmpty(strategy))
    return managers[strategy].use(message);

  logger.info(`No valid strategy found for manager ${message.type}`);
};

module.exports = {
  useStrategy,
};
