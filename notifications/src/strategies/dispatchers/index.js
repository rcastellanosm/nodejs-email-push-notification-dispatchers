const dispatchers = {
  email: require('./email'),
  push: require('./push'),
};
const _ = require('lodash');
const logger = require('../../config/logger');

const useStrategy = (payload) => {
  let message = JSON.parse(payload);
  let strategy = Object.keys(dispatchers).filter(function (dispatch) {
    return dispatch === message.type;
  });

  if (!_.isEmpty(strategy))
    return dispatchers[strategy].use(message);

  logger.info(`No valid strategy found for dispatcher ${payload}`);
};

module.exports = {
  useStrategy,
};
