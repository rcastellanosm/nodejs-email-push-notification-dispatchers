const amqp = require('amqplib');
const { useStrategy } = require('../strategies/managers');

module.exports.start = async () => {
  let queue = process.env.MANAGER_QUEUE;
  let connection = await amqp.connect(process.env.RABBITMQ_DOMAIN);
  let channel = await connection.createChannel();

  await channel.assertQueue(queue, { durable: true });

  channel.consume(queue, async (message) => {
    useStrategy(message.content.toString());
  }, { noAck: true });
};
