const dispatcherWorkerConsumer = require('./dispatcher');
const managerWorkerConsumer = require('./managers');

module.exports = {
  dispatcherWorkerConsumer,
  managerWorkerConsumer,
};
