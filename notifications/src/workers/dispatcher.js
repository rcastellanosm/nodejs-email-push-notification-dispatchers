const amqp = require('amqplib');
const { useStrategy } = require('../strategies/dispatchers');

module.exports.start = async () => {
  let queue = process.env.DISPATCH_NOTIFICATIONS_QUEUE;
  let connection = await amqp.connect(process.env.RABBITMQ_DOMAIN);
  let channel = await connection.createChannel();

  await channel.assertQueue(queue, { durable: true });

  channel.consume(queue, async (message) => {
    useStrategy(message.content.toString());
  }, { noAck: true });
};
