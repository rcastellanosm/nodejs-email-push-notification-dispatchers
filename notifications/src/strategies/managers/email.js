const mgkey = process.env.MAILGUN_SECRET;
const mgDomain = process.env.MAILGUN_DOMAIN;
const mailgun = require('mailgun-js')({ apiKey: mgkey, domain: mgDomain });
const logger = require('../../config/logger');

const use = (message) => {
  if (message.mode === 'unsubscribe')
    return unsubscribe(message);
  return subscribe(message);
};

const subscribe = (message) => {
  let data = {
    address: message.addressee,
    upsert: 'yes',
  };

  mailgun.post('/lists/' + message.list + '/members', data, function (error) {
    if (error)
      throw new Error(`Mailgun list add member error: ${JSON.stringify(error)}`);
    logger.info(`Mailgun successfully add member: ${JSON.stringify(data.address)}`);
  });
};

const unsubscribe = (message) => {
  mailgun.delete('/lists/' + message.list + '/members/' + message.addressee,
    function (error) {
    if (error)
      throw new Error(`Mailgun list remove member error: ${JSON.stringify(error)}`);
    logger.info(`Mailgun successfully remove member: ${JSON.stringify(message.addressee)}`);
  });
};

module.exports = {
  use,
};
