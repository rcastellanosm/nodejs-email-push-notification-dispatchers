const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
const aws = new AWS.SNS({ apiVersion: 'latest' });
const logger = require('../../config/logger');

const use = (message) => {
  if (message.mode === 'unsubscribe')
    return unsubscribe(message);
  return subscribe(message);
};

const unsubscribe = (message) => {
  let topicParams = {
    TopicArn: message.list,
  };
  aws.listSubscriptionsByTopic(topicParams, function (error, data) {
    data.Subscriptions.filter(function (subscription) {
      if (subscription.Endpoint === message.addressee) {
        let params = {
          SubscriptionArn: subscription.SubscriptionArn,
        };

        aws.unsubscribe(params, function (error, data) {
          if (error)
            throw new Error(`AWS SNS unsubscribe user error: ${JSON.stringify(error)}`);
          logger.info(`AWS SNS unsubscribe user successfully: ${JSON.stringify(data.id)}`);
        });
      }
    });
  });
};

const subscribe = (message) => {
  let endpoint = {
    Protocol: 'application',
    TopicArn: message.list,
    Endpoint: message.addressee,
  };

  aws.subscribe(endpoint, function (error, data) {
    if (error)
      throw new Error(`AWS SNS subscribe user error: ${JSON.stringify(error)}`);
    logger.info(`AWS SNS subscribe user successfully: ${JSON.stringify(data)}`);
  });
};

module.exports = {
  use,
};

