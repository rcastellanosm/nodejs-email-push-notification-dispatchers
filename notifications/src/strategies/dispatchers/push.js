const AWS = require('aws-sdk/index');
const logger = require('../../config/logger');

/**
 * AWS-SDK automatically load credentials from this env vars if exists
 *  AWS_SECRET_ACCESS_KEY
 *  AWS_ACCESS_KEY_ID
 */
AWS.config.update({ region: 'us-east-1' });

const use = (message) => {
  let topicType = (message.mode === 'direct') ? 'TargetArn' : 'TopicArn';
  let params = {
    Message: Buffer.from(message.data, 'base64').toString(),
    MessageStructure: 'json',
  };
  params[topicType] = message.addressee;

  let publishTopic = new AWS.SNS({ apiVersion: 'latest' }).publish(params).promise();

  publishTopic.then(function (data) {
      logger.info(`Push Notification dispatch id -> ${data.MessageId}`);
    }).catch(
    function (err) {
      // we must unsubscribe endpoint and add deleted_at now final_user_device
      logger.error(`Push Notification -> ${err.message}`);
    });
};

module.exports = {
  use,
};

