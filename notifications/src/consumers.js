const {
  dispatcherWorkerConsumer,
  managerWorkerConsumer,
} = require('./workers');
const logger = require('./config/logger');

/*
 * Dispatch notification (email or push) to a single recipient /device or a distribution list/topic
 */
dispatcherWorkerConsumer.start().catch((err) => {
  logger.error(`Dispatcher consumer error on start ${err}`);
});

/*
 * Subscribe or Unsubscribe a user from distribution list or topic
 */
managerWorkerConsumer.start().catch((err) => {
  logger.error('Manager consumer error on start: %s', err);
});

