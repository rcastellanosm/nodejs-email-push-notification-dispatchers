const mgkey = process.env.MAILGUN_SECRET;
const mgDomain = process.env.MAILGUN_DOMAIN;
const mailgun = require('mailgun-js')({ apiKey: mgkey, domain: mgDomain });
const logger = require('../../config/logger');

const use = (message) => {
  let data = {
    from: process.env.FROM_EMAIL,
    to: message.addressee,
    subject: message.data.title,
    html: unescape(message.data.html),
  };

  mailgun.messages().send(data, function (error, body) {
    logger.info(`Email Notification dispatch id -> ${body.id}`);
  });
};

module.exports = {
  use,
};
